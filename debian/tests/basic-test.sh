#!/bin/sh

set -e

test_install_python2() {

    echo "Testing python2 package"
    for py in $(pyversions -r 2>/dev/null) ; do
	cd "$AUTOPKGTEST_TMP" ;
	echo "Testing with $py:" ;
	$py -c "import guacamole; print(guacamole)" ;
    done

}

test_install_python3() {

    echo "Testing python3 package"
    for py in $(py3versions -r 2>/dev/null) ; do
	cd "$AUTOPKGTEST_TMP" ;
	echo "Testing with $py:" ;
	$py -c "import guacamole; print(guacamole)" ;
    done

}

###################################
# Main
###################################

for function in "$@"; do
        $function
done
